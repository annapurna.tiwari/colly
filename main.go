package main

import(
  "fmt"
	"io/ioutil"
	// "log"
	// "net/http"
	// "net/http/cookiejar"
	// "net/url"
	// "strings"
  "context"
  "image"
  "bytes"
  "log"
  "os"
  "image/jpeg"
  "github.com/gocolly/colly/v2"
  "github.com/chromedp/chromedp"
  "github.com/signintech/gopdf"
  "github.com/pdfcpu/pdfcpu/pkg/api"
  "github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
  "gitlab.com/annapurna.tiwari/colly/pdf"
  // cdptypes "github.com/chromedp/cdproto/cdp"
)
const (
	baseURL = "https://facebook.com"
)

var (
	username = "annapurnadot@gmail.com"
	password = "your gitlab password"
)

type LoginForm struct {
	Action string `json:"authenticity_token"`
}

func main() {
  // getToken()
  pdf.addWatermark()
  // if err := chromedp.Run(ctx, fullScreenshot(`https://brank.as/`, 90, &buf)); err != nil {
	// 	log.Fatal(err)
	// }
	// if err := ioutil.WriteFile("fullScreenshot.png", buf, 0o644); err != nil {
	// 	log.Fatal(err)
	// }
}

func getToken() LoginForm {
  c := colly.NewCollector()
  ctx, cancel := chromedp.NewContext(
		context.Background(),
		// chromedp.WithDebugf(log.Printf),
	)
	defer cancel()

  var buf []byte
  loginForm := LoginForm{Action: "",}
  c.OnHTML("form[action]", func(e *colly.HTMLElement) {
      loginForm.Action = e.Attr("action")
      data := map[string]string{
    "email": "annapurnadot@gmail.com",
    "pass":   "ChrisGayle@78",
}
      err := c.Post(baseURL+loginForm.Action, data)
      fmt.Println("error:",err)
      // img := cdp.CaptureScreenshot(&buf)
      // fmt.Println(img,"dnjzc",buf)
      if err := chromedp.Run(ctx, elementScreenshot(`https://pkg.go.dev/`, `img.Homepage-logo`, &buf)); err != nil {
		log.Fatal(err)
	}
	if err := ioutil.WriteFile("elementScreenshot.png", buf, 0o644); err != nil {
    log.Fatal(err)
  }
  onTop := false
  wm, _ := pdfcpu.ParseImageWatermarkDetails("watermark.png", "s:1 abs, rot:0", onTop)
  api.AddWatermarksFile("in.pdf", "out.pdf", nil, wm, nil)
  //     cdp.Tasks{
	// 	// cdp.Navigate(`https://www.facebook.com`),
	// 	// cdp.WaitVisible(selpass),
	// 	// cdp.SendKeys(selname, username),
	// 	// cdp.SendKeys(selpass, password),
	// 	// cdp.Submit(selpass),
	// 	// cdp.WaitVisible(`//a[@title="Profile"]`),
	// 	cdp.CaptureScreenshot(&buf)
	// 	cdp.ActionFunc(func(context.Context, cdptypes.Handler) error {
	// 		return ioutil.WriteFile("myfb.png", buf, 0644)
	// 	}),
	// }
  })
  c.OnHTML("a[href='https://www.facebook.com/me/']", func(e *colly.HTMLElement) {
    e.Request.Visit(e.Attr("href"))
  })

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

  c.Visit("https://www.facebook.com")

  fmt.Printf("Action: %s",loginForm.Action)
	return loginForm
}

func elementScreenshot(urlstr, sel string, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(urlstr),
		// chromedp.WaitVisible(sel, chromedp.ByID),
		// chromedp.Screenshot(sel, res, chromedp.NodeVisible, chromedp.ByID),
		chromedp.Screenshot(sel, res, chromedp.NodeVisible),
	}
}

func serveFrames(imgByte []byte) {

    img, _, err := image.Decode(bytes.NewReader(imgByte))
    if err != nil {
        log.Fatalln(err)
    }

    out, _ := os.Create("./img.jpeg")
    defer out.Close()

    var opts jpeg.Options
    opts.Quality = 1

    err = jpeg.Encode(out, img, &opts)
    //jpeg.Encode(out, img, nil)
    if err != nil {
        log.Println(err)
    }

}

func fullScreenshot(urlstr string, quality int64, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(urlstr),
		chromedp.ActionFunc(func(ctx context.Context) error {
			// get layout metrics
			_, _, cssContentSize, err := page.GetLayoutMetrics().Do(ctx)
			if err != nil {
				return err
			}

			width, height := int64(math.Ceil(cssContentSize.Width)), int64(math.Ceil(cssContentSize.Height))

			// force viewport emulation
			err = emulation.SetDeviceMetricsOverride(width, height, 1, false).
				WithScreenOrientation(&emulation.ScreenOrientation{
					Type:  emulation.OrientationTypePortraitPrimary,
					Angle: 0,
				}).
				Do(ctx)
			if err != nil {
				return err
			}

			// capture screenshot
			*res, err = page.CaptureScreenshot().
				WithQuality(quality).
				WithClip(&page.Viewport{
					X:      cssContentSize.X,
					Y:      cssContentSize.Y,
					Width:  cssContentSize.Width,
					Height: cssContentSize.Height,
					Scale:  1,
				}).Do(ctx)
			if err != nil {
				return err
			}
			return nil
		}),
	}
}
