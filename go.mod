module gitlab.com/annapurna.tiwari/colly.git

go 1.14

require (
	github.com/chromedp/cdproto v0.0.0-20210323015217-0942afbea50e
	github.com/chromedp/chromedp v0.6.10
	github.com/gocolly/colly/v2 v2.1.0
	github.com/pdfcpu/pdfcpu v0.3.11
	github.com/signintech/gopdf v0.9.15
)
