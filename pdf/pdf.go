package pdf

import (
	"github.com/pdfcpu/pdfcpu/pkg/api"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
)

func addWatermark() {
	onTop := false
	wm, _ := pdfcpu.ParseImageWatermarkDetails("elementScreenshot.png", "s:1 abs, rot:0", onTop)
	api.AddWatermarksFile("in.pdf", "out.pdf", nil, wm, nil)
}
